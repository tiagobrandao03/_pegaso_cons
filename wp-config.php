<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'pegasoconsultor1' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'U0~&sK&wK|$5/X9#DA[; >K8pe}._VD;cV34B{ey)C89P_Da*_`(iaP?k^krnzWK' );
define( 'SECURE_AUTH_KEY',  '[!y4:=CM.z0ZQ8h:%[R$n6!DCpOW,!NJsoIHiyY0}<A#!#Ra^v(E=|hsix_Dm|_.' );
define( 'LOGGED_IN_KEY',    '_ca_v3Dig>oR@zS/t$h.x5P?uZ<=7>;PsNAsg*&<a(66pQ}6FVBC#ADS1jWH{(4-' );
define( 'NONCE_KEY',        '-z!tj38^Be,)@0|AK[$i}P9fWI5%LdqOo8 #%b0uUR{Es,>p:^,AbW9}!Nbqjtvc' );
define( 'AUTH_SALT',        '>-*~o|Klv_:&Pn}mDYgvqz4}zvXPSK#f9:s{J80Ln>o80e+]IeEA.mY1HEFq]UqD' );
define( 'SECURE_AUTH_SALT', 'm[bQ`B6}L:[c*nmgKcB~y^dn+~1To5dOT,F%Kp^[}mGP=qr`[5dGjG?ejE%wa{62' );
define( 'LOGGED_IN_SALT',   'nsqJI^#z:yc_<P(5b&Ut^91l~^lM2rC.wp72: r=.kOXOj>dHGs^]pexfw(s>nnw' );
define( 'NONCE_SALT',       'Nec:1~@~07G}p6(W-j?Z2<b5K?G}j*!$-DZ:W8yi?Q*mC@V<{e~bI+E|O5Mgrs$w' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
